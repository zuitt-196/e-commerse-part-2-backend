const User = require('../models/User');
const bcrypt = require('bcrypt'); // define the hashed the password or convert into complex as purpose of secure
const jwt = require('jsonwebtoken'); // authentaction for user


// if you notice the process of build API endpoint we going to use the merge process which is combination of route and contoller in one file 

const authController = require('express').Router()



// CRAETE  OR GENERATE TOKEN 

const createToken = (user) => {
    // console.log("user:",user)
    const payload = {
        id: user._id.toString(),
        email:user.email,
    }

    const token = jwt.sign(payload, process.env.JWT_SECRET, {expiresIn: '5h'});
    
    return token
}



// REGISTRATION CONTROLLER AND ROUTE OF USER
authController.post("/register", async(req, res) => {
        // const {username,email, password} = req.body
    try {
        const isExiting = await User.findOne({email: req.body.email})
        // validation if the user is exist 
        if (isExiting) {
            return res.status(500).json({message: 'User has been already registered'})
        }

        // validation if the user is no fields pupulated
        if (req.body.username === "" || req.body.email === "" || req.body.password === "") {
                return res.status(500).json({message: 'All fields must  be populated'})
    }
        
        
        //  hashed the password
        const hashedPassword = await (await bcrypt.hash(req.body.password, 10));

        console.log(hashedPassword)
         

        // example 1234 -> hashed dsadsadas980d0a8242342134
        const user  = await User.create({...req.body, password: hashedPassword});
         await user.save();

         // dsfracture the the user used of ._doc method which means the setter of internal mongoose , with define the new object propperty 
         const {password, ...others} = user._doc;
         
        // pass thhe generate token 
         const token = createToken(others);


         // SEND INTO CLIENT SERVER
         return res.status(201).json({others, token})
        

    } catch (error) {
        return res.status(500).json(error.message)
    }
})



// LOGIN  CONTROLLER AND ROUTE OF USER

authController.post('/login', async(req, res)=>{
    console.log("logiAPIreq:",req.headers.authorization);
    const {email, password} = req.body;

    if (email === ""|| password === "") {
        return res.status(500).json({message: "All fields must be populated"})
    }


    

    try {
        const user = await User.findOne({email})
        if (!user) {
            return res.status(500).json({message: "Invalid credential I gues wrong your email"})

        }
        
        // compare the regular password and  hashedPassword 
        const comparePass  = await bcrypt.compare(req.body.password, user.password)
        if (!comparePass ) {
            return res.status(500).json({message: "Invalid credential I gues wrong your password"})
        }

        const {password, ...others} = user._doc;
        // 
        const token = createToken(user);

        // SEND INTO CLIENT SERVER
        return res.status(200).json({others, token})
        
    } catch (error) {
            return res.status(500).json(error.message)  
    }
})



module.exports = authController