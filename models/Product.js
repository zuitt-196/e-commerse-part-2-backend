const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
        title:{
                type:String,
                required: [true, "plase add title of the product"],
                unique: true,
        },
        desc:{
                type:String,
                required: [true, "plase add description of the product"],
           
        },

        firsImg:{
                type:String,
                required: [true, "plase add first image of the product"],

 
        },
        secondImg:{
            type:String,
             required: [true, "plase add second  image of the product"],   
        },

        price: {
                type:String,
                required: [true, "plase add price of the product"],
        },
        stars:{
            type:Number,
            default:3
        }
  
            
},
    
        {
            timestamps:true,
        }  
)

module.exports= mongoose.model('Product',ProductSchema);