const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
        username:{
                type:String,
                required: [true, "plase add your name"],
                unique: true,
        },
        email:{
                type:String,
                required: [true, "plase add your email"],
                unique: true,
        },

        password:{
                type:String,
                required: [true, "plase add your password"],

        }

},
    {
        timestamps:true,
    }    
)

module.exports= mongoose.model('User',UserSchema);