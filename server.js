const express = require('express');
const dotenv  = require('dotenv').config();
const mongoose = require('mongoose')
const cors = require('cors')

// IMPORT CONTROLLER 
const authController = require("./controllers/authController");
const productController = require("./controllers/productController");
const uploadController = require('./controllers/uploadController');


mongoose.set('strictQuery',false);

const app = express()

// INTERGATION ON DATABASE/MONMGO
mongoose.connect(process.env.MONGO_URL, ()=>{
        console.log("DATABASE IS SUCESSFULLY CONNECTED");
})

// MIDLEWARE SECTION TO TRACK IN COMMMING REQUEST FROM CLIENT
app.use(cors()); 
app.use(express.json());
app.use(express.urlencoded({ extended:false}));




app.use('/auth', authController);
app.use('/product', productController);
app.use('/uploaded', uploadController);

// NOTIFCATION IF THE SERVER IS G OOD
app.listen(process.env.PORT || 5000, () => {
        console.log("NAG RUN NA ANG  SERVER  VHONGS !!!!")
})